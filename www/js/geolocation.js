/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },


    // GEOLOCATIONAPI:7/8 << END
    initMap: function(lat2, lon2) {
         lat1 = 48.868904;
         lon1 = 2.337638;

         //lat2 = 48.871307;
         //lon2 = 2.339563;

function distanceFrom(points) {
    var lat1 = points.lat1;
    var radianLat1 = lat1 * (Math.PI / 180);
    var lng1 = points.lng1;
    var radianLng1 = lng1 * (Math.PI / 180);
    var lat2 = points.lat2;
    var radianLat2 = lat2 * (Math.PI / 180);
    var lng2 = points.lng2;
    var radianLng2 = lng2 * (Math.PI / 180);
    var earth_radius = 6371;
    var diffLat = (radianLat1 - radianLat2);
    var diffLng = (radianLng1 - radianLng2);
    var sinLat = Math.sin(diffLat / 2);
    var sinLng = Math.sin(diffLng / 2);
    var a = Math.pow(sinLat, 2.0) + Math.cos(radianLat1) * Math.cos(radianLat2) * Math.pow(sinLng, 2.0);
    var distance = earth_radius * 2 * Math.asin(Math.min(1, Math.sqrt(a)));
    return distance.toFixed(3);
}

var distance = distanceFrom({
    'lat1': lat1,
    'lng1': lon1,

    'lat2': lat2,
    'lng2': lon2
});

var map = document.getElementById('mapCoordinates');
map.innerHTML = distance;

if(distance > 1){
    alert("Tu n'est pas à l'IESA!")
}

/*
        var map = document.getElementById('mapCoordinates');
        map.innerHTML = 'lat:' + lat + ' / long:' + long;
        var options = {
            zoom: 4,
            center: new google.maps.LatLng(lat, long),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map'), options);
        var markerPoint = new google.maps.LatLng(lat, long);
        var marker = new google.maps.Marker({
                                            position: markerPoint,
                                            map: map,
                                            title: 'Device\'s Location'
                                            });
*/
    },
    onMapSuccess: function(position){
        var coords = position.coords;
        app.initMap(coords.latitude, coords.longitude);
    },
    onMapFailure: function(error){
        // navigator.notification.alert(error.message, null);
        alert("onMapFailure :: " + error.message);
    },
    setGeolocation: function() {
        navigator.geolocation.getCurrentPosition(app.onMapSuccess, app.onMapFailure, {timeout: 5000, enableAccuracy: true});
    },
    // GEOLOCATIONAPI:7/8 << END



    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // GEOLOCATIONAPI:6/8
        var setGeolocation = document.getElementById('setGeolocation');
        setGeolocation.addEventListener('click', function(event) { app.setGeolocation(event); }, true);

        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
