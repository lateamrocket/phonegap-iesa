/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var watchID = null;
var watchIDAcc = null;
var previousAcceleration = { x: null, y: null, z: null };
var online = 0;

var app = {

    // Application Constructor
    initialize: function() {
        //var document.jsonContacts = null;
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    // GEOLOCATIONAPI:7/8 << BEGIN
    initMap: function(lat2, lon2) {
         lat1 = 48.868904;
         lon1 = 2.337638;

         var latitude = localStorage.getItem("latitude");
         var longitude = localStorage.getItem("longitude");

         if( latitude && longitude ){

            if(latitude == 0 && longitude == 0){
                lat2 = lat1;
                lon2 = lon1;
            } else {
                lat2 = latitude;
                lon2 = longitude;
            }
            
         }

        function distanceFrom(points) {
            var lat1 = points.lat1;
            var radianLat1 = lat1 * (Math.PI / 180);
            var lng1 = points.lng1;
            var radianLng1 = lng1 * (Math.PI / 180);
            var lat2 = points.lat2;
            var radianLat2 = lat2 * (Math.PI / 180);
            var lng2 = points.lng2;
            var radianLng2 = lng2 * (Math.PI / 180);
            var earth_radius = 6371;
            var diffLat = (radianLat1 - radianLat2);
            var diffLng = (radianLng1 - radianLng2);
            var sinLat = Math.sin(diffLat / 2);
            var sinLng = Math.sin(diffLng / 2);
            var a = Math.pow(sinLat, 2.0) + Math.cos(radianLat1) * Math.cos(radianLat2) * Math.pow(sinLng, 2.0);
            var distance = earth_radius * 2 * Math.asin(Math.min(1, Math.sqrt(a)));
            return distance.toFixed(3);
        }

        var distance = distanceFrom({
            'lat1': lat1,
            'lng1': lon1,

            'lat2': lat2,
            'lng2': lon2
        });

        var map = document.getElementById('mapCoordinates');
        map.innerHTML = distance;

        if(distance > 1){
            localStorage.setItem("iesa","false");
            //alert("Tu n'est pas à l'IESA! (tu es a "+ distance +"km du 5,rue saint augustin)")
        } else{
            localStorage.setItem("iesa","true");
            //alert("Tu es à l'IESA!")
        }

    },
    onGeolocationSuccess: function(position){
        var coords = position.coords;
        app.initMap(coords.latitude, coords.longitude);
    },
    onGeolocationFailure: function(error){
        // navigator.notification.alert(error.message, null);
        //alert("onMapFailure :: " + error.message);
    },
    setGeolocation: function() {
        navigator.geolocation.getCurrentPosition(app.onGeolocationSuccess, app.onGeolocationFailure, {timeout: 5000, enableAccuracy: true});
    },

    setPosition: function(){
        var _ = document.webL10n.get;

        var latitude = document.getElementById('latitude').value;
        var longitude = document.getElementById('longitude').value;

        localStorage.setItem("latitude", latitude);
        localStorage.setItem("longitude", longitude);

        app.vibrate();
        alert(_('SavePositionOk'));
    },


    //CAMERA
    getPicture: function(){
        var _ = document.webL10n.get;
        var iesa = localStorage.getItem("iesa");

        if(iesa == "true"){

                // We are safe to use the camera 
                navigator.camera.getPicture(onSuccess, onFail, { 
                    quality: 60, 
                    destinationType: Camera.DestinationType.DATA_URL,
                    encodingType: Camera.EncodingType.JPEG,
                    saveToPhotoAlbum: true,
                    sourceType : Camera.PictureSourceType.CAMERA,
                });

                //destinationType: Camera.DestinationType.FILE_URI });

                function onSuccess(imageData) {

                    var image = document.getElementById('myImage');
                    image.src = "data:image/jpeg;base64," + imageData;
                    //image.src = imageURI;

                    app.saveimageSQL(imageData);
                }

                function onFail(message) {
                    //alert('Failed because: ' + message);
                    
                    //pas de camera on prend la photo dans un album photo
                    navigator.camera.getPicture(onSuccess, onFail, { 
                        quality: 60, 
                        destinationType: Camera.DestinationType.DATA_URL,
                        encodingType: Camera.EncodingType.JPEG,
                        saveToPhotoAlbum: false,
                        sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
                    });

                    //app.vibrate();
                    //alert(_('CameraFail'));
                }

        } else {
            app.vibrate();
            alert(_('GeolocationNotIESA'))
        }

    },

    // CONTACTSAPI:6/7 << BEGIN
    loadContact: function() {

        var contactEmailElement = document.getElementById('contactEmails');
        contactEmailElement.innerHTML = 'Loading...';

        var contactName = document.getElementById('contactName').value;

        var options = new ContactFindOptions();
        options.filter = contactName;
        options.multiple = true;
        var fields = ["displayName", "nickname", "name", "phoneNumbers", "emails"];
        navigator.contacts.find(fields, this.onContactFindSuccess, this.onContactFindError, options);
    },
    saveContact: function(event, idContact) {
        // alert("pass");
        // alert(idContact);

        var name = $(".contactName"+idContact).text();
        var surname = $(".contactSurname"+idContact).text();
        var fonction = $(".contactFunction"+idContact).text();
        var email = $(".contactEmail"+idContact).text();
        var tel = $(".contactTel"+idContact).text();

        var contact = navigator.contacts.create();


        if (device.platform == "Android") {
            contact.rawId = idContact+123456;
        } else {
            contact.id = idContact+'iesa';
        }
        
        contact.displayName = name;
        contact.nickname = name;

        var contactName = new ContactName();
        contactName.givenName = surname;
        contactName.familyName = name;
        contact.name = contactName;

        var organizations = [];
        organizations[0] = new ContactOrganization(false, 'work', 'IESA','',fonction); // preferred
        contact.organizations = organizations;

        var emails = [];
        emails[0] = new ContactField('PhoneGape', email, true); // preferred
        contact.emails = emails;

        var phoneNumbers = [];
        phoneNumbers[0] = new ContactField('work', tel, true); // preferred
        contact.phoneNumbers = phoneNumbers;

        // alert(JSON.stringify(contact));

        contact.save(this.onContactSaved, this.onContactSavedError);
        // contact.remove(onContactRemoved, onContactRemovedError);
    },
    onContactSavedError : function(error) {
        var _ = document.webL10n.get;
        //alert("onContactSavedError :: " + error.code);
        app.vibrate();
        alert(_("ContactSavedError"));
    },
    onContactSaved : function() {
        var _ = document.webL10n.get;
        app.vibrate();
        alert(_("ContactSaved"));
    },
    /*
    onContactFindSuccess: function(contacts) {
        var contactEmailsElement = document.getElementById('contactEmails');
        contactEmailsElement.innerHTML = '';
        for (var i = 0; i < contacts.length; i++) {
            var contact = contacts[i];
            var opt = document.createElement('li');
            opt.setAttribute('value', contact.displayName);
            opt.setAttribute('class', 'ui-li-static ui-body-inherit ui-last-child');
            //if (contact.displayName) opt.innerHTML = opt.innerHTML + '#' + contact.displayName;
            //if (contact.nickName) opt.innerHTML = opt.innerHTML + '#' + contact.nickName;
            //if (contact.name.givenName) opt.innerHTML = opt.innerHTML + ' # ' + contact.name.givenName;
            //if (contact.name.familyName) opt.innerHTML = opt.innerHTML + ' # ' + contact.name.familyName;
            var email = '';
            for (var j = 0; j < contact.emails.length; j++) {
                email = contact.emails[j].value;
            }
            opt.innerHTML = contact.name.givenName + ' ' + contact.name.familyName + ' : ' + email;
            contactEmailsElement.appendChild(opt);
        }
        var opt = document.createElement('li');
        opt.setAttribute('value', 'Loaded!');
        opt.setAttribute('class', 'ui-li-static ui-body-inherit ui-last-child');
        opt.innerHTML = 'Loaded!';
        contactEmailsElement.appendChild(opt);
    },
    onContactFindError : function(contactError) {
        alert("onContactFindError :: " + contactError.code);
    },
    */
    // CONTACTSAPI:7/7 << END

    // DATABASEAPI:3/3 << BEGIN
    prepareDatabase: function() {
        var name = 'images';
        var version = '0.1';
        var display_name = 'My images';
        var size = (1024 * 1024 * 40); // 40 Mo
        app.database = window.openDatabase(name, version, display_name, size);
        app.database.transaction(this.createTables, this.onCreateTableError, this.onCreateTableSuccess);
    },
    createTables: function(tx) {
        //tx.executeSql('DROP TABLE IF EXISTS text');
        //tx.executeSql('DROP TABLE IF EXISTS images');
        tx.executeSql('CREATE TABLE IF NOT EXISTS text (id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS images (id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT)');
    },
    onCreateTableError : function(error) {
        // alert("onCreateTableError :: Error processing SQL: " + error.code);
    },
    onCreateTableSuccess : function() {
        //alert("onCreateTableSuccess !");
    },
    loadSQL: function() {
        app.database.transaction(function(tx){
            var sql = 'SELECT * FROM text;';
            tx.executeSql(sql, [], function(txObj, result){
                var nicknameOptions = document.querySelector('#typedNicknamesSQL');
                nicknameOptions.innerHTML = '';
                var limit = result.rows.length;
                for (i = 0; i < limit; i++){
                    var obj = result.rows.item(i);
                    var opt = document.createElement('li');
                    opt.setAttribute('value', obj.id);
                    opt.setAttribute('class', 'ui-li-static ui-body-inherit ui-last-child');
                    opt.innerHTML = obj.value;
                    nicknameOptions.appendChild(opt);
                }
            }, null);
        }, this.onTransactionFault, null);
    },
    loadRandomPicture: function() {
        app.database.transaction(function(tx){
            var sql = 'SELECT * FROM images;';
            tx.executeSql(sql, [], function(txObj, result){
                var limit = result.rows.length;
                
                if(limit == 1){
                    var random  = 0;
                } else {
                    var random  = Math.floor(Math.random() * limit);
                }
                
                if(limit == 0){
                    var _ = document.webL10n.get;
                    alert(_('NoPhotos'));
                } else {
                    var obj = result.rows.item(random);
                    //alert(random);
                    //alert(result.rows.item(random).value);
                    $("#randomcontent div").html('<img style="width:100%;" src="data:image/jpeg;base64,'+obj.value+'" />');
                }
                
            }, null);
        }, this.onTransactionFault, null);
    },
    loadPhotos: function() {
        
        app.database.transaction(function(tx){
            var sql = 'SELECT * FROM images;';
            tx.executeSql(sql, [], function(txObj, result){
                var limit = result.rows.length;
                //alert(limit);
                if(limit == 0){
                    var _ = document.webL10n.get;
                    alert(_('NoPhotos'));
                } else {
                    for (i = 0; i < limit; i++){
                        var obj = result.rows.item(i);

                        //alert('<img src="data:image/jpeg;base64,'+obj.value+'" />');
                        if($("#photo"+i).length == 0){    
                            $("#photoscontent").append('<img width="80" id="photo'+i+'" src="data:image/jpeg;base64,'+obj.value+'" />');
                        }
                    }
                }

            }, null);

        }, this.onTransactionFault,null ); //this.onTransactionSuccess

    },
    saveSQL: function() {
        var nickname = document.getElementById('nicknameSQL');
        var newValue = nickname.value;
        nickname.value = '';

        app.database.transaction(function(tx) {
            var sql = 'INSERT INTO text (value) VALUES (?);';
            tx.executeSql(sql, [newValue]);
        }, this.onTransactionFault, null);
        app.loadSQL();
    },
    saveimageSQL: function(imageData) {

        app.database.transaction(function(tx) {
            var sql = 'INSERT INTO images (value) VALUES (?);';
            tx.executeSql(sql, [imageData]);
        }, this.onTransactionFault, this.onCreateTableSuccess);

    },
    onTransactionFault: function(error) {
        app.vibrate();
        //alert("onTransactionFault :: Error processing SQL: " + error.code);
    },
    onTransactionSuccess: function(error) {
        //alert("onTransactionSuccess ");
    },
    // DATABASEAPI:3/3 << END

    // CONNECTIONAPI:3/3 << BEGIN
    checkConnection: function() {
        var _ = document.webL10n.get;
        var networkState = navigator.connection.type;
    
        var states = {};
        states[Connection.UNKNOWN]  = 0;
        states[Connection.ETHERNET] = 1;
        states[Connection.WIFI]     = 1;
        states[Connection.CELL_2G]  = 1;
        states[Connection.CELL_3G]  = 1;
        states[Connection.CELL_4G]  = 1;
        states[Connection.CELL]     = 1;
        states[Connection.NONE]     = 0;

        var onlineApp;
        var urlJson = 'js/contacts.json';

        if (states[networkState] == 1) {
            online = 1;

            //ANALYTICS
            analytics.startTrackerWithId('UA-50964173-1');
            analytics.trackView('home');
            
            urlJson = 'http://jsonp.jit.su/?url=http://gist.githubusercontent.com/felixRottier/b3dd9d9b74b60c923264/raw/contacts.json';
        }

        $(document).ready(function(){
            var jsonContacts = null;

            if(!jsonContacts){

                $.ajax({
                  type: 'GET',
                  url: urlJson,
                  dataType: 'json',
                  timeout: 3000,
                  success: function(json){
                    if(json.length > 2){
                        //alert("json online")
                        jsonContacts = json;
                    }
                    testJson()
                  },
                  error: function() {
                    //alert('La requête n\'a pas abouti');
                    testJson()
                 }
                });

                function testJson(){
                     if(!jsonContacts){
                        urlJson = 'js/contacts.json';
                        $.getJSON(urlJson, function(json){
                            app.vibrate();
                            
                            jsonContacts = json;
                            displayJson()
                        });
                    } else {
                        displayJson()
                    }
                }

                function displayJson(){
                    if(online == 0){
                        alert(_("LocalJson"), "");
                    }
                    $.each(jsonContacts,function(i, value){
                        $('#contactsIESA').append(
                            '<ul class="contactIesa"><li>' +
                            _('Lastname') + ' : <span class="contactName'+i+'">'+ value.nom + '</span></li>' +
                            '<li class="contactSurname">'+ _('Firstname') + ' : <span class="contactSurname'+i+'">'+ value.prenom + '</span></li>' +
                            '<li class="contactFunction">' + _('Fonction') + ' : <span class="contactFunction'+i+'">'+ value.fonction + '</span></li>' +
                            '<li>' + _('Mail')+ ' : <a href="mailto:'+ value.mail +'"><span class="contactEmail'+i+'">'+ value.mail + '</span></a></li>' +
                            '<li>'+ _('Telephone') +' : <a href="tel:'+ value.tel +'"><span class="contactTel'+i+'">'+ value.tel + '</span></a></li>' +
                            '<li id="'+i+'" class="saveContactButton">'+ _('SaveContact') +'</li></ul>'
                        );
                    });
                }

            }
     
        });

        
    }, 
    onDeviceOnline: function() {
        app.checkConnection();
    },
    onDeviceOffline: function() {
        app.checkConnection();
    },
    // CONNECTIONAPI:3/3 << END

    //GLOBALIZATION
    onLocalized: function() {
        var l10n = document.webL10n;

        navigator.globalization.getPreferredLanguage(
            function (language) {
                //alert(language.value);
                if(language.value == "fr" || language.value == "français"){
                    l10n.setLanguage('fr');
                } else {
                    l10n.setLanguage('en-US');
                }
            },
            function () {
                //alert("error language, en default");
                l10n.setLanguage('en-US');
            }
        );
        //alert(l10n.getLanguage());
    },

    //VIBRATION
    vibrate: function() {
        navigator.notification.vibrate(300);
    },

    //ACCELERATION
    watchAcceleration: function() {
        var options = { frequency: 300 };
        watchIDAcc = navigator.accelerometer.watchAcceleration(this.watchAccelerationSuccess, this.watchAccelerationError, options);
    },
    watchAccelerationSuccess: function(acc) {
        var accelerationChange = {};
        if (previousAcceleration.x !== null) {
            accelerationChange.x = Math.abs(previousAcceleration.x, acc.x);
            accelerationChange.y = Math.abs(previousAcceleration.y, acc.y);
            accelerationChange.z = Math.abs(previousAcceleration.z, acc.z);
        }

        if (accelerationChange.x + accelerationChange.y + accelerationChange.z > 35) {
            // Shake detected
            //alert('shakeeee')
            app.loadRandomPicture();

            app.stopWatchAcceleration();
            setTimeout(app.watchAcceleration(), 1000);
            previousAcceleration = { 
                x: null, 
                y: null, 
                z: null
            }
        } else {
            previousAcceleration = {
                x: acc.x,
                y: acc.y,
                z: acc.z
            }
        }
        /*
        alert('Acceleration X: ' + acc.x + '\n' +
                  'Acceleration Y: ' + acc.y + '\n' +
                  'Acceleration Z: ' + acc.z + '\n' +
                  'Timestamp: '      + acc.timestamp + '\n');
*/
    },
    watchAccelerationError: function(acc) {
        var _ = document.webL10n.get;
        alert(_('AccIncompatible'));
    },
    stopWatchAcceleration: function () {
        if (watchIDAcc !== null) {
            navigator.accelerometer.clearWatch(watchIDAcc);
            watchIDAcc = null;
        }
    },
    // Compass
    // onSuccess: Get the current heading
    //
    startWatch: function() {
        // Update compass every 2 seconds
        var options = { frequency: 2000 };
        watchID = navigator.compass.watchHeading(this.onSuccessWatch, this.onErrorWatch, options);
    },
    // Stop watching the compass
    //
    stopWatch: function() {
        if (watchID) {
            navigator.compass.clearWatch(watchID);
            watchID = null;
        }
    },
    // onSuccess: Get the current heading
    //
    onSuccessWatch: function(heading) {
      // alert("success");
        // var element = document.getElementById('heading');
        // element.innerHTML = 'Heading: ' + heading.magneticHeading;
    },
    // onError: Failed to get the heading
    //
    onErrorWatch: function(compassError) {
        // alert('Compass error: ' + compassError.code);
    },


    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'   
    onDeviceReady: function() {

        document.webL10n.ready(app.onLocalized);
        app.checkConnection();
        app.setGeolocation();

         // DATABASEAPI:2/3
        app.prepareDatabase();
        var saveNicknameSQL = document.getElementById('saveNicknameSQL');
        saveNicknameSQL.addEventListener('click', function(event) { app.saveSQL(event); }, true);

        // CONNECTIONAPI:2/3
        document.addEventListener("online", this.onDeviceOnline);
        document.addEventListener("offline", this.onDeviceOffline);

        // CAMERAAPI
        var setGeolocation = document.getElementById('getPicture');
        setGeolocation.addEventListener('click', function(event) { app.getPicture(event); }, true);

        // GEOLOCATIONAPI:6/8
        var setGeolocation = document.getElementById('setGeolocation');
        setGeolocation.addEventListener('click', function(event) { app.setGeolocation(event); }, true);

        var setPosition = document.getElementById('saveCoordinates');
        setPosition.addEventListener('click', function(event) { app.setPosition(event); }, true);

        //PHOTOS
        var photos = document.getElementById('photosbtn');
        photos.addEventListener('click', function(event) { app.loadPhotos(event); }, true);

        //RANDOM
        var random = document.getElementById('randombtn');
        random.addEventListener('click', function(event) { app.loadRandomPicture(event); }, true);

         //VIBRATION
        var vibrate = document.getElementById('vibrate');
        vibrate.addEventListener('click', function(event) { app.vibrate(event); }, true);


        // CONTACTSAPI:5/7
        // var loadContactButton = document.getElementById('loadContactButton');
        // loadContactButton.addEventListener('click', function(event) { app.loadContact(event); }, true);
        // var saveContactButton = $("#saveContactButton");
        $("body").on("click", ".saveContactButton", function(event)
        { 
            var idContact = $(this).attr("id");
            app.saveContact(event, idContact); 
        });

        //ACCELERATION
        app.watchAcceleration();

        // Compass
        app.startWatch();

        document.addEventListener('deviceready', function () {
            watchId = navigator.compass.watchHeading(function (heading) {
            // only magnetic heading works universally on iOS and Android
            // round off the heading then trigger newHeading event for any listeners
            var newHeading = Math.round(heading.magneticHeading);
            // $("body").trigger("newHeading", [newHeading]);
            $("#compass").trigger("newHeading", [newHeading]);
            // $compass.css('-webkit-transform', updateCompass);
            }, function (error) {
            // if we get an error, show its code
            // alert("Compass error: " + error.code);
            }, {frequency : 300});
        });
        
    }
};
