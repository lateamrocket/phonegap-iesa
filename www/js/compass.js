/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


var watchID = null;

var app = {

    // Application Constructor
    initialize: function() {
        //var document.jsonContacts = null;
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    // GEOLOCATIONAPI:7/8 << BEGIN
    initMap: function(lat2, lon2) {
         lat1 = 48.868904;
         lon1 = 2.337638;

         lat2 = lat1;
         lon2 = lon1;
         
         //lat2 = 48.871307;
         //lon2 = 2.339563;
         
         //lat2 = 49.8689041;
         //lon2 = 2.337638;

        function distanceFrom(points) {
            var lat1 = points.lat1;
            var radianLat1 = lat1 * (Math.PI / 180);
            var lng1 = points.lng1;
            var radianLng1 = lng1 * (Math.PI / 180);
            var lat2 = points.lat2;
            var radianLat2 = lat2 * (Math.PI / 180);
            var lng2 = points.lng2;
            var radianLng2 = lng2 * (Math.PI / 180);
            var earth_radius = 6371;
            var diffLat = (radianLat1 - radianLat2);
            var diffLng = (radianLng1 - radianLng2);
            var sinLat = Math.sin(diffLat / 2);
            var sinLng = Math.sin(diffLng / 2);
            var a = Math.pow(sinLat, 2.0) + Math.cos(radianLat1) * Math.cos(radianLat2) * Math.pow(sinLng, 2.0);
            var distance = earth_radius * 2 * Math.asin(Math.min(1, Math.sqrt(a)));
            return distance.toFixed(3);
        }

        var distance = distanceFrom({
            'lat1': lat1,
            'lng1': lon1,

            'lat2': lat2,
            'lng2': lon2
        });

        var map = document.getElementById('mapCoordinates');
        map.innerHTML = distance;

        if(distance > 1){
            localStorage.setItem("iesa","false");
            //return false;
            //alert("Tu n'est pas à l'IESA! (tu es a "+ distance +"km du 5,rue saint augustin)")
        } else{
            localStorage.setItem("iesa","true");
            //alert("Tu es à l'IESA!")
            //return true;
        }

    },
    onMapSuccess: function(position){
        var coords = position.coords;
        app.initMap(coords.latitude, coords.longitude);
    },
    onMapFailure: function(error){
        // navigator.notification.alert(error.message, null);
        alert("onMapFailure :: " + error.message);
    },
    setGeolocation: function() {
        alert("Set Get Location");
        navigator.geolocation.getCurrentPosition(app.onMapSuccess, app.onMapFailure, {timeout: 5000, enableAccuracy: true});
    },

    getPicture: function(){

        var iesa = localStorage.getItem("iesa");

        if(iesa == "true"){

            if (typeof navigator.camera !== "undefined") { 
                // We are safe to use the camera 

                navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
                
                destinationType: Camera.DestinationType.FILE_URI });

                function onSuccess(imageURI) {
                    var image = document.getElementById('myImage');
                    image.src = imageURI;
                }

                function onFail(message) {
                    alert('Failed because: ' + message);
                }

            } else{
                alert('Pas de camera');
            }

        } else {
            alert("Tu n'es pas à l'IESA! Pas de photo !")
        }

    },
    // GEOLOCATIONAPI:7/8 << END

    // CONTACTSAPI:6/7 << BEGIN
    loadContact: function() {
        var contactEmailElement = document.getElementById('contactEmails');
        contactEmailElement.innerHTML = 'Loading...';

        var contactName = document.getElementById('contactName').value;

        var options = new ContactFindOptions();
        options.filter = contactName;
        options.multiple = true;
        var fields = ["displayName", "nickname", "name", "phoneNumbers", "emails"];
        navigator.contacts.find(fields, this.onContactFindSuccess, this.onContactFindError, options);
    },
    saveContact: function(event, idContact) {
        // alert("pass");
        // alert(idContact);

        var name = $(".contactName"+idContact).text();
        var surname = $(".contactSurname"+idContact).text();
        var fonction = $(".contactFunction"+idContact).text();
        var email = $(".contactEmail"+idContact).text();
        var tel = $(".contactTel"+idContact).text();
        
        // var contact = navigator.contacts.create({'displayName': 'Giorgio'});
        var contact = navigator.contacts.create();
        contact.id = idContact + "iesa";
        contact.displayName = name;
        // contact.nickname = name;

        var contactName = new ContactName();
        contactName.givenName = surname;
        contactName.familyName = name;
        contact.name = contactName;

        var organizations = [];
        organizations[0] = new ContactOrganization(false, 'work', 'IESA','',fonction); // preferred
        contact.organizations = organizations;

        var emails = [];
        emails[0] = new ContactField('PhoneGape', email, true); // preferred
        contact.emails = emails;

        var phoneNumbers = [];
        phoneNumbers[0] = new ContactField('PhoneGape', tel, true); // preferred
        contact.phoneNumbers = phoneNumbers;

        // alert(JSON.stringify(contact));

        contact.save(this.onContactSaved, this.onContactSavedError);
        // contact.remove(onContactRemoved, onContactRemovedError);
    },
    onContactSavedError : function(error) {
        alert("onContactSavedError :: " + error.code);
    },
    onContactSaved : function() {
        alert("Contact Saved !");
    },
    onContactFindSuccess: function(contacts) {
        var contactEmailsElement = document.getElementById('contactEmails');
        contactEmailsElement.innerHTML = '';
        for (var i = 0; i < contacts.length; i++) {
            var contact = contacts[i];
            var opt = document.createElement('li');
            opt.setAttribute('value', contact.displayName);
            opt.setAttribute('class', 'ui-li-static ui-body-inherit ui-last-child');
            //if (contact.displayName) opt.innerHTML = opt.innerHTML + '#' + contact.displayName;
            //if (contact.nickName) opt.innerHTML = opt.innerHTML + '#' + contact.nickName;
            //if (contact.name.givenName) opt.innerHTML = opt.innerHTML + ' # ' + contact.name.givenName;
            //if (contact.name.familyName) opt.innerHTML = opt.innerHTML + ' # ' + contact.name.familyName;
            var email = '';
            for (var j = 0; j < contact.emails.length; j++) {
                email = contact.emails[j].value;
            }
            opt.innerHTML = contact.name.givenName + ' ' + contact.name.familyName + ' : ' + email;
            contactEmailsElement.appendChild(opt);
        }
        var opt = document.createElement('li');
        opt.setAttribute('value', 'Loaded!');
        opt.setAttribute('class', 'ui-li-static ui-body-inherit ui-last-child');
        opt.innerHTML = 'Loaded!';
        contactEmailsElement.appendChild(opt);
    },
    onContactFindError : function(contactError) {
        alert("onContactFindError :: " + contactError.code);
    },
    // CONTACTSAPI:7/7 << END

    // DATABASEAPI:3/3 << BEGIN
    prepareDatabase: function() {
        var name = 'friends';
        var version = '0.1';
        var display_name = 'My Friends';
        var size = (1024 * 1024 * 2); // 2 Mo
        app.database = window.openDatabase(name, version, display_name, size);
        app.database.transaction(this.createTables, this.onCreateTableError, this.onCreateTableSuccess);
    },
    createTables: function(tx) {
        // tx.executeSql('DROP TABLE IF EXISTS FRIENDS');
        tx.executeSql('CREATE TABLE IF NOT EXISTS FRIENDS (id INTEGER PRIMARY KEY AUTOINCREMENT, nickname TEXT)');
        // tx.executeSql('INSERT INTO FRIENDS (nickname) VALUES ("Batman")');
        // tx.executeSql('INSERT INTO FRIENDS (nickname) VALUES ("Robin")');
    },
    onCreateTableError : function(error) {
        alert("onCreateTableError :: Error processing SQL: " + error.code);
    },
    onCreateTableSuccess : function() {
        alert("onCreateTableSuccess !");
    },
    loadSQL: function() {
        app.database.transaction(function(tx){
                                  var sql = 'SELECT * FROM friends;';
                                  tx.executeSql(sql, [], function(txObj, result){
                                                var nicknameOptions = document.querySelector('#typedDescription');
                                                nicknameOptions.innerHTML = '';
                                                var limit = result.rows.length;
                                                for (i = 0; i < limit; i++){
                                                    var obj = result.rows.item(i);
                                                    var opt = document.createElement('li');
                                                    opt.setAttribute('value', obj.id);
                                                    opt.setAttribute('class', 'ui-li-static ui-body-inherit ui-last-child');
                                                    opt.innerHTML = obj.nickname;
                                                    nicknameOptions.appendChild(opt);
                                                }
                                            }, null);
                                  }, this.onTransactionFault, null);
    },
    saveSQL: function() {
        var nickname = document.getElementById('descritpionStorage');
        var newValue = nickname.value;
        nickname.value = '';

        app.database.transaction(function(tx) {
                                    var sql = 'INSERT INTO friends (nickname) VALUES (?);';
                                    tx.executeSql(sql, [newValue]);
                                  }, this.onTransactionFault, null /*this.renderData*/);
        app.loadSQL();
    },
    onTransactionFault: function(error) {
        alert("onTransactionFault :: Error processing SQL: " + error.code);
    },
    renderData: function(event) {
    },
    // DATABASEAPI:3/3 << END

    // CONNECTIONAPI:3/3 << BEGIN
    checkConnection: function() {
        var networkState = navigator.connection.type;
    
        var states = {};
        states[Connection.UNKNOWN]  = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI]     = 'WiFi connection';
        states[Connection.CELL_2G]  = 'Cell 2G connection';
        states[Connection.CELL_3G]  = 'Cell 3G connection';
        states[Connection.CELL_4G]  = 'Cell 4G connection';
        states[Connection.CELL]     = 'Cell generic connection';
        states[Connection.NONE]     = 'No network connection';
    
        alert('Connection type: ' + states[networkState]);

        var onlineApp;
        var urlJson = 'js/contacts.json';

        if (states != states[Connection.NONE])
        {
            onlineApp = 1;
            urlJson = 'http://jsonp.jit.su/?url=http://gist.githubusercontent.com/felixRottier/b3dd9d9b74b60c923264/raw/contacts.json';
        }

        $(document).ready(function(){
            var jsonContacts = null;

            if(!jsonContacts){

                $.ajax({
                  type: 'GET',
                  url: urlJson,
                  dataType: 'json',
                  timeout: 3000,
                  success: function(json){
                    if(json.length > 2){
                        alert("json online")
                        jsonContacts = json;
                    }
                    testJson()
                  },
                  error: function() {
                    alert('La requête n\'a pas abouti');
                    testJson()
                 }
                });

                function testJson(){
                     if(!jsonContacts){
                        urlJson = 'js/contacts.json';
                        $.getJSON(urlJson, function(json){
                            alert("json hors ligne")
                            jsonContacts = json;
                            displayJson()
                        });
                    } else {
                        displayJson()
                    }
                }

                function displayJson(){
                    var _ = document.webL10n.get;
                    $.each(jsonContacts,function(i, value){
                        $('#contactsIESA').append(
                            '<ul class="contactIesa"><li>' +
                            _('Lastname') + ' : <span class="contactName'+i+'">'+ value.nom + '</span></li>' +
                            '<li class="contactSurname">'+ _('Firstname') + ' : <span class="contactSurname'+i+'">'+ value.prenom + '</span></li>' +
                            '<li class="contactFunction">' + _('Fonction') + ' : <span class="contactFunction'+i+'">'+ value.fonction + '</span></li>' +
                            '<li>' + _('Mail')+ ' : <span class="contactEmail'+i+'">'+ value.mail + '</span></li>' +
                            '<li>'+ _('Telephone') +' : <span class="contactTel'+i+'">'+ value.tel + '</span></li>' +
                            '<li id="'+i+'" class="saveContactButton">'+ _('SaveContact') +'</li></ul>'
                        );
                    });
                }

            }
     
        });

        
    }, 
    onDeviceOnline: function() {
        app.checkConnection();
        //alert('pas de connexion donc event');
    },
    onDeviceOffline: function() {
        app.checkConnection();
    },
    // CONNECTIONAPI:3/3 << END


    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onLocalized: function() {
        var l10n = document.webL10n;

        navigator.globalization.getPreferredLanguage(
            function (language) {
                //alert(language.value);
                if(language.value == "fr" || language.value == "français"){
                    l10n.setLanguage('fr');
                } else {
                    l10n.setLanguage('en-US');
                    //l10n.setLanguage('fr');
                }
                //app.checkConnection();
            },
            function () {
                alert("error language, en default");
                l10n.setLanguage('en-US');
                //app.checkConnection();
            }
        );
        
        //alert(l10n.getLanguage());
    },

    // Compass
    // onSuccess: Get the current heading
    //
    startWatch: function() {
      alert("Start Watch");

        // Update compass every 3 seconds
        var options = { frequency: 3000 };

        watchID = navigator.compass.watchHeading(this.onSuccessWatch, this.onErrorWatch, options);
    },

    // Stop watching the compass
    //
    stopWatch: function() {
        alert("Stop Watch");
        if (watchID) {
            navigator.compass.clearWatch(watchID);
            watchID = null;
        }
    },

    // onSuccess: Get the current heading
    //
    onSuccessWatch: function(heading) {
      // alert("success");
        var element = document.getElementById('heading');
        element.innerHTML = 'Heading: ' + heading.magneticHeading;
    },

    // onError: Failed to get the heading
    //
    onErrorWatch: function(compassError) {
        alert('Compass error: ' + compassError.code);
    },
        
    onDeviceReady: function() {

        document.webL10n.ready(app.onLocalized);

        app.setGeolocation();

        //ANALYTICS
        analytics.startTrackerWithId('UA-50964173-1');
        analytics.trackView('home');

        // CAMERAAPI
        var setGeolocation = document.getElementById('getPicture');
        setGeolocation.addEventListener('click', function(event) { app.getPicture(event); }, true);


        // GEOLOCATIONAPI:6/8
        var setGeolocation = document.getElementById('setGeolocation');
        setGeolocation.addEventListener('click', function(event) { app.setGeolocation(event); }, true);

        alert("6");

        // CONTACTSAPI:5/7
        // var loadContactButton = document.getElementById('loadContactButton');
        // loadContactButton.addEventListener('click', function(event) { app.loadContact(event); }, true);
        // var saveContactButton = $("#saveContactButton");
        $("body").on("click", ".saveContactButton", function(event)
        { 
            var idContact = $(this).attr("id");
            app.saveContact(event, idContact); 
        });

        // CONNECTIONAPI:2/3
        document.addEventListener("online", this.onDeviceOnline);
        document.addEventListener("offline", this.onDeviceOffline);
        

        // DATABASEAPI:2/3
        // app.prepareDatabase();
        // var saveNicknameSQL = document.getElementById('saveDescription');
        // saveNicknameSQL.addEventListener('click', function(event) { console.log('test'); app.saveSQL(event); }, true);
        // app.loadSQL();

        app.checkConnection();

        // Compass
        app.startWatch();

        document.addEventListener('deviceready', function () {
            RocknCoder.Compass.watchId = navigator.compass.watchHeading(function (heading) {
            // only magnetic heading works universally on iOS and Android
            // round off the heading then trigger newHeading event for any listeners
            var newHeading = Math.round(heading.magneticHeading);
            // $("body").trigger("newHeading", [newHeading]);
            $("#compassImage").trigger("newHeading", [newHeading]);
            // $compass.css('-webkit-transform', updateCompass);
        }, function (error) {
            // if we get an error, show its code
            // alert("Compass error: " + error.code);
        }, {frequency : 100});
});

        app.receivedEvent('deviceready');
    },
    
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);

        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};


