﻿// create our namespace
var RocknCoder = RocknCoder || {};

// event handlers for the compass stuff,
// one for updating the header text
// the other for rotating the compass
RocknCoder.Compass = (function () {
	// alert("3");
	var lastHeading = -1;
		// cache the jQuery selectors
		// $headText = $("#header > h1");
		$compass = $("#compassImage");
		// displays the degree
		// updateHeadingText = function (event, heading) {
		// 	event.preventDefault();
		// 	alert("update Heading Text");
		// 	$headText.html(heading + "&deg;");
		// 	return false;
		// },
		// adjusts the rotation of the compass

		// updateHeadingText = function (event, heading) {
		// 	event.preventDefault();
		// 	$headText.html(heading + "&deg;");
		// 	return false;
		// },
		// adjusts the rotation of the compass
		updateCompass = function (event, heading) {
			event.preventDefault();
			// to make the compass dial point the right way
			var rotation = 360 - heading,
			rotateDeg = 'rotate(' + rotation + 'deg)';
			// TODO: fix - this code only works on webkit browsers, not wp7
			$compass.css('-webkit-transform', rotateDeg);
			return false;
		};
	// bind both of the event handlers to the "newHeading" event
	$("body").bind("newHeading", updateCompass);
	// $("body").bind("newHeading", updateCompass).bind("newHeading", updateHeadingText);
}());